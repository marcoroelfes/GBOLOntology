package life.gbol.prepare;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.HashMap;

import org.codehaus.plexus.util.FileUtils;

import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;


public class Build
{
  public static void main(String args[]) throws Exception
  {
    FileUtils.deleteDirectory(new File("../GBOLApi/src/main/java/life/gbol/domain"));
    args = new String[]{"-i","root-ontology.ttl","GbolAdditional.ttl","-o","../GBOLApi","-r","../RGBOLApi","-owl","generated/GBOL.owl","-ShExC","generated/GBOL.shexc","-rg","generated/GBOL_RDF2Graph.ttl"};//"-skipNarrowingProperties"
//    args = new String[]{"-i","root-ontology.ttl","GbolAdditional.ttl","-o","../GBOLApi","-r","../RGBOLApi","-rg","overview/GBOLRDF2Graph.ttl","-skipNarrowingProperties"};
    org.empusa.codegen.Main.main(args);
    buildEnumList();
  }

  public static void tbol(String args[]) throws Exception
  {
    FileUtils.deleteDirectory(new File("../TBOLApi/src/main/java/life/gbol/domain"));
    args = new String[]{"-i","root-ontology.ttl","TBOL.ttl","TbolAdditional.ttl","-o","../TBOLApi","-r","../RTBOLApi","-rg","generated/TBOLRDF2Graph.ttl","-skipNarrowingProperties"};
    if(new File("../sapp/Conversion/conversion/").exists())
    {
      FileUtils.copyFile(new File("generated/GBOL_JSONLDFrame.json"),
                       new File("../sapp/Conversion/conversion/src/main/resources/GBOL_JSONLDFrame.json"));
    }
    org.empusa.codegen.Main.main(args);
  }
  
  public static void buildEnumList() throws Exception
  {
    Writer writer = new FileWriter("generated/EnumProps.tsv");
    RDFSimpleCon con = new RDFSimpleCon("file://root-ontology.ttl");
    HashMap<String,String> checkSingleDef = new HashMap<String,String>();
    for(ResultLine line : con.runQuery("getEnums.sparql",false))
    {
      String enumType = line.getIRI("enumType");
      String items = line.getIRI("items");
      String parent = line.getIRI("parent");
      String label = line.getLitString("label");
      boolean defaultVal = line.getLitBoolean("default",false);
      
      if(parent.equals(enumType))
        parent = "";
      
      enumType = enumType.substring("http://gbol.live/0.1/".length());
      if(defaultVal)
      {
        if(checkSingleDef.containsKey(enumType) && !checkSingleDef.get(enumType).equals(items)) 
        {
          writer.close();
          throw new Exception("Only one default value allowed: " + enumType);
        }
        checkSingleDef.put(enumType,items);
      }
      String writeLine = enumType + "\t" + items + "\t" + label + "\t" + parent + "\t" + defaultVal + "\n";
      System.out.println(writeLine);
      writer.write(writeLine);
          
      //?enumType ?items ?parent ?label
    }
    writer.close();
  }

}
