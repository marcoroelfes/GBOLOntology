#!/bin/bash
#============================================================================
#title          :GBOL Installation
#description    :GBOL Ontology installation script
#author         :Jasper Koehorst
#date           :2016
#version        :0.0.1
#============================================================================
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# git -C $DIR pull # Not functional in private repo's yet...

if [ "$1" == "test" ]; then
	gradle build -b "$DIR/build.gradle" --info
else
	echo "Skipping tests, run './install.sh test' to perform tests"
	gradle build -b "$DIR/build.gradle" -x test
fi

mv $DIR/build/libs/*jar $DIR/
