#!/bin/bash
#============================================================================
#title          :GBOL Build
#description    :GBOLApi build script
#author         :Jasper Koehorst
#date           :2016
#version        :0.0.1
#============================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#Update local repo and ontology
git -C $DIR pull

# Create directory by git pull...
git clone https://gitlab.com/gbol/GBOLApi ../GBOLApi
git clone https://gitlab.com/gbol/RGBOLApi ../RGBOLApi

#RTBOLApi does not exist yet at gitlab
git clone https://gitlab.com/gbol/TBOLApi ../TBOLApi

#Cleaning the dir for re-initiation
rm -rf ../GBOLApi/src/*
rm -rf ../RGBOLApi/R/*
rm -rf ../TBOLApi/src/*

# Runs the test files which builds the API
gradle cleanTest test -b "$DIR/build.gradle" --info 

# Add all files
#cd ../GBOLApi && git add . && git commit -m "GBOLApi update" && git push
#cd ../RGBOLApi && git add . && git commit -m "GBOLApi update" && git push
#cd ../TBOLApi && git add . && git commit -m "T/GBOLApi update" && git push
#cd ../gbolontology